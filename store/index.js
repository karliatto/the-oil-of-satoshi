export const state = () => ({
  products: [
    {
      id: 1,
      title: 'Arbequina',
      description: 'Organic extra virgin olive oil | Bottle of 250 ml',
      longDescription: `A delicate and fresh oil; intense green fruit with notes of green almonds, tomato, banana and apple giving medium levels of bitterness and pepper. This oil bursts with surprising nuances and shades of flavour. We strongly recommend using this oil fresh on salads, rice dishes, soups and sauces where it will bring out the natural flavours of the food while adding a subtle something extra. Try it in your desserts or cocktails!`,
      price: 50,
      ratings: 3,
      reviews: 5,
      isAddedToCart: false,
      isAddedBtn: false,
      isFavourite: false,
      quantity: 1,
      image: 'arbequina.png',
    },
    {
      id: 2,
      title: 'Picual',
      description: 'Organic extra virgin olive oil | Bottle of 250 ml',
      longDescription:
        'Suggestive sensations with notes of tomato, fig and freshly cut grass with a balanced flavor, between bitter and spicy, that conquers the palate. This oil has an excellent combination of natural fatty acids, a high polyphenol content, vitamin E and a good resistance to oxidisation making it especially important for health care and the preservation of foodstuffs. It is a perfect accompaniment for both fish and meat dishes, stir-fries and all types of sauces and emulsions.',
      price: 35,
      ratings: 5,
      reviews: 10,
      isAddedToCart: false,
      isAddedBtn: false,
      isFavourite: false,
      quantity: 1,
      image: 'https://bulma.io/images/placeholders/1280x960.png',
    },
    {
      id: 3,
      title: 'Cornicabra',
      description: 'Organic extra virgin olive oil | Bottle of 250 ml ',
      longDescription:
        'The Cornicabra variety is unique, it is very agreeable on the palate with a surprising combination of an almost sweet entrance and powerful finish. This variety produces one of the most stable oils that exist, it has a high concentration of monounsaturated oleic acid, vitamin E and natural antioxidants to support the cardiovascular system. Ideal in tortillas, with shellfish or meat and especially in stews and casseroles.',
      price: 35,
      ratings: 5,
      reviews: 10,
      isAddedToCart: false,
      isAddedBtn: false,
      isFavourite: false,
      quantity: 1,
      image: 'https://bulma.io/images/placeholders/1280x960.png',
    },
  ],
  userInfo: {
    isLoggedIn: false,
    isSignedUp: false,
    hasSearched: false,
    name: '',
    productTitleSearched: '',
  },
  systemInfo: {
    openLoginModal: false,
    openRegistrationModal: false,
    openCheckoutModal: false,
  },
});

export const getters = {
  productsAdded: state => {
    return state.products.filter(el => {
      return el.isAddedToCart;
    });
  },
  productsAddedToFavourite: state => {
    return state.products.filter(el => {
      return el.isFavourite;
    });
  },
  getProductById: state => id => {
    console.log('id', id);
    console.log('state', state);
    return state.products.find(product => product.id === +id);
  },
  isUserLoggedIn: state => {
    return state.userInfo.isLoggedIn;
  },
  isUserSignedUp: state => {
    return state.userInfo.isSignedUp;
  },
  getUserName: state => {
    return state.userInfo.name;
  },
  isLoginModalOpen: state => {
    return state.systemInfo.openLoginModal;
  },
  isRegistrationModalOpen: state => {
    return state.systemInfo.openRegistrationModal;
  },
  isCheckoutModalOpen: state => {
    return state.systemInfo.openCheckoutModal;
  },
  quantity: state => {
    return state.products.quantity;
  },
};

export const mutations = {
  addToCart: (state, id) => {
    state.products.forEach(el => {
      if (id === el.id) {
        el.isAddedToCart = true;
      }
    });
  },
  setAddedBtn: (state, data) => {
    state.products.forEach(el => {
      if (data.id === el.id) {
        el.isAddedBtn = data.status;
      }
    });
  },
  removeFromCart: (state, id) => {
    state.products.forEach(el => {
      if (id === el.id) {
        el.isAddedToCart = false;
      }
    });
  },
  removeProductsFromFavourite: state => {
    state.products.filter(el => {
      el.isFavourite = false;
    });
  },
  isUserLoggedIn: (state, isUserLoggedIn) => {
    state.userInfo.isLoggedIn = isUserLoggedIn;
  },
  isUserSignedUp: (state, isSignedUp) => {
    state.userInfo.isSignedUp = isSignedUp;
  },
  setHasUserSearched: (state, hasSearched) => {
    state.userInfo.hasSearched = hasSearched;
  },
  setUserName: (state, name) => {
    state.userInfo.name = name;
  },
  setProductTitleSearched: (state, titleSearched) => {
    state.userInfo.productTitleSearched = titleSearched;
  },
  showLoginModal: (state, show) => {
    state.systemInfo.openLoginModal = show;
  },
  showRegistrationModal: (state, show) => {
    console.log('showRegistrationModal');

    state.systemInfo.openRegistrationModal = show;
    console.log('state.systemInfo', state.systemInfo);
  },
  showCheckoutModal: (state, show) => {
    state.systemInfo.openCheckoutModal = show;
  },
  addToFavourite: (state, id) => {
    state.products.forEach(el => {
      if (id === el.id) {
        el.isFavourite = true;
      }
    });
  },
  removeFromFavourite: (state, id) => {
    state.products.forEach(el => {
      if (id === el.id) {
        el.isFavourite = false;
      }
    });
  },
  quantity: (state, data) => {
    state.products.forEach(el => {
      if (data.id === el.id) {
        el.quantity = data.quantity;
      }
    });
  },
  SET_USER(state, authUser) {
    state.authUser = authUser;
  },
};
/*
export const actions = {
  async nuxtServerInit({ commit }) {
    const res = await this.$axios.get("/api/current_user")
    commit("SET_USER", res.data)
  },

  async logout({ commit }) {
    const { data } = await this.$axios.get("/api/logout")
    if (data.ok) commit("SET_USER", null)
  },

  async handleToken({ commit }, token) {
    const res = await this.$axios.post("/api/stripe", token)
    commit("SET_USER", res.data)
  }
} */
